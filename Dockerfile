FROM amazon/aws-cli:2.0.59

RUN yum install -y gcc openssl-devel bzip2-devel libffi-devel gzip wget make tar

RUN wget https://www.python.org/ftp/python/3.8.6/Python-3.8.6.tgz \
&& tar xzf Python-3.8.6.tgz \
&& cd Python-3.8.6 \
&& ./configure --enable-optimizations \
&& make altinstall

RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash - \
&& yum install -y nodejs

RUN npm install -g serverless
ENTRYPOINT [ "/bin/bash", "-c" ]
SHELL ["/bin/bash", "-c"]